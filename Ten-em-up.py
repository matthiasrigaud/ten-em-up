#!/usr/bin/env python3

##
#  Main file
##

import pygame
import sys

sys.path.append('.')

from src import Constant, Event, TenSeconds, Player, Sky, Enemy, Barrels, StartScreen, EndScreen, ScoreFile

if __name__ == '__main__':

    pygame.init()
    screen = pygame.display.set_mode((Constant.WIN_WIDTH, Constant.WIN_HEIGHT))
    pygame.display.set_caption(Constant.WIN_TITLE)
    pygame.mixer.music.load(Constant.MUSIC_PATH)
    pygame.mixer.music.play(-1)

    old_time = pygame.time.get_ticks()
    ten_seconds_timer = TenSeconds.TenSeconds()
    player = Player.Player()
    sky = Sky.Sky()
    enemy_manager = Enemy.EnemyManager()
    barrels_interface = Barrels.Interface()
    scores = ScoreFile.ScoreFile()
    start_screen = StartScreen.StartScreen(scores)
    end_screen = EndScreen.EndScreen(scores)

    pygame.mouse.set_cursor(*pygame.cursors.diamond)
    state = 0

    while 1:
        cur_time = pygame.time.get_ticks()
        elapsed_time = cur_time - old_time
        old_time = cur_time

        #
        # look at events
        #

        Event.manager(start_screen if state == 0 else end_screen if state == 2 else None)

        #
        # update
        #

        sky.update(elapsed_time)
        if state == 0:
            if start_screen.update(elapsed_time):
                state = 1
                pygame.mixer.music.stop()
                pygame.mixer.music.play(-1)
        elif state == 1:
            ten_seconds_timer.update(elapsed_time)
            if not player.update(elapsed_time):
                state = 2
                end_screen.set_score(player.score)
                continue
            barrels_interface.update(elapsed_time)
            new_barrels = enemy_manager.update(elapsed_time)
            for pos in new_barrels:
                barrels_interface.spawn_barrel(pos)
            enemy_manager.check_collisions(player)
            barrels_interface.collide_with_barrels(player)
            if ten_seconds_timer.event():
                ten_seconds_timer.reset()
                barrels_interface.change_effects()
                player.score = (player.score + Constant.SCORE_TEN_SEC) * Constant.SCORE_TEN_SEC_COEF
        elif state == 2:
            if end_screen.update(elapsed_time):
                state = 0
                old_time = pygame.time.get_ticks()
                ten_seconds_timer = TenSeconds.TenSeconds()
                player = Player.Player()
                enemy_manager = Enemy.EnemyManager()
                barrels_interface = Barrels.Interface()

        #
        # draw
        #

        sky.draw_background(screen)
        if state == 0:
            start_screen.draw(screen)
        elif state == 1 or state == 2:
            enemy_manager.draw(screen)
            barrels_interface.draw_barrels(screen)
            player.draw(screen)
            sky.draw_foreground(screen)
            if state == 2:
                end_screen.draw(screen)
        ten_seconds_timer.draw(screen, [10, 10])
        barrels_interface.draw_interface(screen, [150, 10])
        player.draw_stats(screen, [10, 120], [10, 150])
        pygame.display.flip()

        #
        # wait
        #

        pygame.time.wait(Constant.FRAME_TIME - (pygame.time.get_ticks() - cur_time))
