##
#  implement the enemies
##

import pygame
from . import Constant, Bullet, DamageMask
from random import random, randrange
from math import sqrt, cos, sin


class EnemyManager:

    class Enemy:

        SKIN = [
            pygame.transform.smoothscale(pygame.image.load(Constant.ENEMY_SKIN_1), [105, 60]),
            pygame.transform.smoothscale(pygame.image.load(Constant.ENEMY_SKIN_2), [45, 23])
        ]

        MASKS = [
            DamageMask.generate_damage_mask(SKIN[0], [255, 0, 0], 100),
            DamageMask.generate_damage_mask(SKIN[1], [255, 0, 0], 100),
        ]

        def __init__(self, enemy_type):
            self.TYPE = enemy_type
            self.kamikaze = False
            self.damage_blink_speed = 100
            self.damage_blink_timer = 0
            self.damage_effect_timer = 0
            self.display_damage = False
            margin = 0.1

            self.position = [
                Constant.WIN_WIDTH + self.SKIN[self.TYPE].get_width() / 2,
                randrange(int(self.SKIN[self.TYPE].get_height() * (margin + 0.5)), int(Constant.WIN_HEIGHT - self.SKIN[self.TYPE].get_height() * (0.5 + margin)))
            ]

            self.life = Constant.ENEMY_LIFE[self.TYPE]

            self.speed = [-100, 0]
            self.action_cooldown = 0
            self.shift_cooldown = 0
            self.shift_direction = 0

        def take_damage(self, damage):
            self.life -= damage
            self.display_damage = True
            self.damage_effect_timer = 500

        def update(self, elapsed_time):
            if self.damage_effect_timer > 0:
                self.damage_blink_timer += elapsed_time
                self.damage_effect_timer -= elapsed_time
                if self.damage_blink_timer >= self.damage_blink_speed:
                    self.display_damage = not self.display_damage
                    self.damage_blink_timer = 0
                if self.damage_effect_timer <= 0:
                    self.display_damage = False
                    self.damage_blink_timer = 0
                    self.damage_effect_timer = 0
            projectile = None
            self.action_cooldown += elapsed_time
            self.shift_cooldown -= elapsed_time

            if self.shift_cooldown <= 0:
                self.shift_direction = 0
                self.shift_cooldown = 0

            if self.action_cooldown >= Constant.ENEMY_ACT_FRQ[self.TYPE]:
                self.action_cooldown = 0
                if self.TYPE == 0:
                    projectile = Bullet.Bullet(2, [self.position[0] + self.SKIN[self.TYPE].get_width() / 2,
                                                   self.position[1]],
                                               [-800, 0])
                elif self.TYPE == 1:
                    projectile = Bullet.Bullet(1, [self.position[0] + self.SKIN[self.TYPE].get_width() / 2,
                                                   self.position[1]],
                                               [-650, 0])
                    self.shift_cooldown = Constant.ENEMY_ACT_FRQ[self.TYPE] / 8
                    self.shift_direction = Constant.ENEMY_SHIFT_SPD * (-1 if randrange(0, 2) == 1 else 1)

            self.position[0] += self.speed[0] * elapsed_time / 1000
            self.position[1] += (self.speed[1] + self.shift_direction) * elapsed_time / 1000
            if self.position[1] < 0:
                self.position[1] = 0
                self.shift_direction *= -1
            elif self.position[1] > Constant.WIN_HEIGHT - self.SKIN[self.TYPE].get_height():
                self.position[1] = Constant.WIN_HEIGHT - self.SKIN[self.TYPE].get_height()
                self.shift_direction *= -1
            return projectile

        def draw(self, screen):
            screen.blit(self.SKIN[self.TYPE], self.get_rect())
            if self.display_damage:
                screen.blit(self.MASKS[self.TYPE], self.get_rect())
            if Constant.SHOW_HITBOX:
                pygame.draw.rect(screen, [255, 0, 0], self.get_rect(), width=5)

        def get_rect(self):
            return pygame.Rect([self.position[0] - self.SKIN[self.TYPE].get_width() / 2,
                                self.position[1] - self.SKIN[self.TYPE].get_height() / 2],
                               [self.SKIN[self.TYPE].get_width(), self.SKIN[self.TYPE].get_height()])

        def collision(self, rect):
            return self.get_rect().colliderect(rect)

        def looting(self):
            if self.life <= 0 and not self.kamikaze:
                return True
            return False

        def should_be_delete(self):
            if self.life <= 0 or self.position[0] <= - self.SKIN[self.TYPE].get_width():
                return True
            return False

    def __init__(self):
        self.enemies = []
        self.enemy_bullets = []

    def update(self, elapsed_time):
        barrels_to_create = []

        if Constant.ENEMY_COEF * elapsed_time >= random():
            self.enemies.append(self.Enemy(randrange(0, 2)))

        new_enemies = []
        for enemy in self.enemies:
            bullet = enemy.update(elapsed_time)
            if bullet:
                self.enemy_bullets.append(bullet)
            if not enemy.should_be_delete():
                new_enemies.append(enemy)
            elif enemy.looting():
                for i in range(Constant.ENEMY_LOOT[enemy.TYPE]):
                    barrels_to_create.append(list(enemy.position))

        self.enemies = new_enemies

        new_bullets = []
        for bullet in self.enemy_bullets:
            bullet.update(elapsed_time)
            if not bullet.should_be_deleted():
                new_bullets.append(bullet)
        self.enemy_bullets = new_bullets
        return barrels_to_create

    def check_collisions(self, player):

        for pbullet in player.bullets:
            for ebullet in self.enemy_bullets:
                if pbullet.collision(ebullet.get_rect()):
                    pbullet.life -= ebullet.damage()
                    ebullet.life -= pbullet.damage()
                    break
            if pbullet.life <= 0:
                break
            for enemy in self.enemies:
                if pbullet.collision(enemy.get_rect()):
                    pbullet.life -= enemy.life
                    enemy.take_damage(pbullet.damage())
                    if enemy.life <= 0:
                        player.score += Constant.SCORE_KILL[enemy.TYPE]
        for ebullet in self.enemy_bullets:
            if ebullet.life >= 0 and ebullet.collision(player.get_rect()):
                player.take_damage(ebullet.damage())
                ebullet.life = 0

        for enemy in self.enemies:
            if enemy.collision(player.get_rect()):
                player.take_damage(enemy.life)
                enemy.life = 0
                enemy.kamikaze = True
                break

    def draw(self, screen):
        for enemy in self.enemies:
            enemy.draw(screen)
        for bullet in self.enemy_bullets:
            bullet.draw(screen)
