##
#  generate mask to apply an effect on damage
##

import pygame


def generate_damage_mask(pict: pygame.Surface, color, alpha):
    mask = pict.copy()
    mask.fill(color)
    mask.set_alpha(alpha)
    alpha_color = list(color)
    alpha_color.append(0)
    for x in range(pict.get_width()):
        for y in range(pict.get_height()):
            alpha_color[3] = pict.get_at((x, y)).a
            mask.set_at((x, y), alpha_color)
    return mask
