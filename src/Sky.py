##
#  Manage the background
##

import pygame
from . import Constant
from random import randrange, random


class Sky:

    class Cloud:

        CLOUDS = [
            pygame.image.load(Constant.CLOUD_1),
            pygame.image.load(Constant.CLOUD_2),
            pygame.image.load(Constant.CLOUD_3),
            pygame.image.load(Constant.CLOUD_4),
        ]

        def __init__(self):
            self.TYPE = randrange(0, len(self.CLOUDS))
            self.is_foreground = True if self.TYPE <= 1 and random() < 0.5 else False
            self.speed = randrange(Constant.CLOUD_MIN_SPEED, Constant.CLOUD_MAX_SPEED)

            tolerance = int(self.CLOUDS[self.TYPE].get_height() / 2)
            self.position = [
                Constant.WIN_WIDTH,
                randrange(-tolerance, Constant.WIN_HEIGHT - tolerance)
            ]

        def update(self, elapsed_time):
            self.position[0] -= self.speed * elapsed_time / 1000

        def draw(self, screen):
            screen.blit(self.CLOUDS[self.TYPE], self.position)

        def foreground_cloud(self):
            return self.is_foreground

        def should_be_deleted(self):
            if self.position[0] <= - self.CLOUDS[self.TYPE].get_width():
                return True
            return False

    def __init__(self):
        self.clouds = []

        # simulate 10 seconds game to have a cloudy sky since beggining
        for i in range(Constant.FRAME_TIME, 10000, Constant.FRAME_TIME):
            self.update(i)

    def update(self, elapsed_time):
        if Constant.CLOUD_COEF * elapsed_time >= random() and Constant.MAX_CLOUDS > len(self.clouds):
            self.clouds.append(self.Cloud())

        new_clouds = []
        for cloud in self.clouds:
            cloud.update(elapsed_time)
            if not cloud.should_be_deleted():
                new_clouds.append(cloud)

        self.clouds = new_clouds

    def draw_background(self, screen):
        screen.fill(Constant.SKY_COLOR)
        for cloud in self.clouds:
            if not cloud.foreground_cloud():
                cloud.draw(screen)

    def draw_foreground(self, screen):
        for cloud in self.clouds:
            if cloud.foreground_cloud():
                cloud.draw(screen)
