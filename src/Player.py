##
#  Ship controlled by player
##
from typing import Union, List

import pygame
from math import sqrt, pi, cos, sin, trunc
from . import Constant, Bullet, Enemy, DamageMask


class Player:

    def __init__(self):
        self.bullets = []
        self.position = [0, Constant.WIN_HEIGHT / 2]
        self.skin = pygame.transform.smoothscale(pygame.image.load(Constant.PLAYER_SKIN), [154, 40])
        self.damage_mask = DamageMask.generate_damage_mask(self.skin, [255, 0, 0], 100)
        self.damage_blink_speed = 100
        self.damage_blink_timer = 0
        self.damage_effect_timer = 0
        self.display_damage = False
        self.shoot_cooldown = 0
        self.score = 0
        self.font = pygame.font.Font(Constant.FONT_PATH, 15)

        ##
        #  all those stats could be increased or decreased during game
        ##
        self.weapon_level = 0
        self.type_weapon = 0
        self.life = Constant.MAX_PLAYER_LIFE
        self.speed = 500  # pixels per second
        self.shoot_speed = 500
        self.bullet_speed = [
            [800, 0],
            [650, 0]
        ]
        # type 0 is not quantifiable nor spreadable
        self.bullet_quantity = [
            1,
            1
        ]
        self.bullet_spread = [
            None,
            pi / 4
        ]

    def shoot(self, bullet_type):
        if bullet_type == 0:
            for i in range(self.bullet_quantity[bullet_type]):
                position = [self.position[0] + self.skin.get_width() / 2, self.position[1]]
                i_modifier = 0
                y_modifier = 0
                if self.bullet_quantity[bullet_type] % 2 == 0:
                    i_modifier = 2
                    y_modifier = -Bullet.Bullet.BULLET_SKINS[bullet_type].get_height() / 2
                if i % 2 == 0:
                    position[0] -= (Bullet.Bullet.BULLET_SKINS[bullet_type].get_width() / 2) * ((i + i_modifier) / 2)
                    position[1] += y_modifier + Bullet.Bullet.BULLET_SKINS[bullet_type].get_height() * ((i + i_modifier) / 2)
                else:
                    position[0] -= (Bullet.Bullet.BULLET_SKINS[bullet_type].get_width() / 2) * ((i + 1) / 2)
                    position[1] -= y_modifier + Bullet.Bullet.BULLET_SKINS[bullet_type].get_height() * ((i + 1) / 2)
                self.bullets.append(Bullet.Bullet(bullet_type, position, self.bullet_speed[bullet_type]))
        elif bullet_type == 1:
            if self.bullet_quantity[bullet_type] > 1:
                angle = self.bullet_spread[bullet_type] / 2
            else:
                angle = 0
            for i in range(self.bullet_quantity[bullet_type]):
                speed = list(self.bullet_speed[bullet_type])
                cos_angle = cos(angle)
                sin_angle = sin(angle)
                speed[0] = int(cos_angle * speed[0] - sin_angle * speed[1])
                speed[1] = int(sin_angle * speed[0] + cos_angle * speed[1])
                self.bullets.append(Bullet.Bullet(bullet_type,
                                                  [self.position[0] + self.skin.get_width() / 2,
                                                   self.position[1]],
                                                  speed))
                if self.bullet_quantity[bullet_type] > 1:
                    angle -= self.bullet_spread[bullet_type] / (self.bullet_quantity[bullet_type] - 1)

    def take_damage(self, damage):
        self.life -= damage
        self.display_damage = True
        self.damage_effect_timer = 500

    def update(self, elapsed_time):
        if self.damage_effect_timer > 0:
            self.damage_blink_timer += elapsed_time
            self.damage_effect_timer -= elapsed_time
            if self.damage_blink_timer >= self.damage_blink_speed:
                self.display_damage = not self.display_damage
                self.damage_blink_timer = 0
            if self.damage_effect_timer <= 0:
                self.display_damage = False
                self.damage_blink_timer = 0
                self.damage_effect_timer = 0
        if self.life <= 0:
            return False
        mouse = pygame.mouse.get_pos()

        # vector player -> cursor
        to_mouse = [mouse[0] - self.position[0], mouse[1] - self.position[1]]
        dist = sqrt(to_mouse[0] ** 2 + to_mouse[1] ** 2)

        if dist >= 100:
            move = self.speed * elapsed_time / 1000
        else:
            move = self.speed * dist * elapsed_time / (1000 * 100)

        if move < dist:
            self.position[0] += (to_mouse[0] / dist) * move
            self.position[1] += (to_mouse[1] / dist) * move
        else:
            self.position = list(mouse)

        new_bullets = []
        for bullet in self.bullets:
            bullet.update(elapsed_time)
            if not bullet.should_be_deleted():
                new_bullets.append(bullet)
        self.bullets = new_bullets

        self.shoot_cooldown += elapsed_time
        if self.shoot_cooldown >= self.shoot_speed:
            self.shoot_cooldown = 0
            self.shoot(self.type_weapon)
        return True

    def get_rect(self):
        return pygame.Rect([self.position[0] - self.skin.get_width() / 2, self.position[1] - self.skin.get_height() / 2],
                           [self.skin.get_width(), self.skin.get_height()])

    def collision(self, rect):
        return self.get_rect().colliderect(rect)

    def draw(self, screen):
        screen.blit(self.skin, self.get_rect())
        if self.display_damage:
            screen.blit(self.damage_mask, self.get_rect())
        if Constant.SHOW_HITBOX:
            pygame.draw.rect(screen, [255, 0, 0], self.get_rect(), width=5)
        for bullet in self.bullets:
            bullet.draw(screen)

    def draw_stats(self, screen, position_score, position_life):
        color = [90, 90, 170]
        text_surface_score = self.font.render("Score : %d" % self.score, True, color)
        text_surface_life = self.font.render("Life : %d" % self.life, True, color)
        screen.blit(text_surface_score, position_score)
        screen.blit(text_surface_life, position_life)
