##
#  screen displayed after player lost
##

import pygame
from . import Constant


class EndScreen:

    LOSE_MSG = "Game Over"
    END_MSG = "Try again!"

    def __init__(self, scores):
        self.scores = scores
        lose_font = pygame.font.Font(Constant.FONT_PATH, 70)
        self.msg_font = pygame.font.Font(Constant.FONT_PATH, 25)
        self.color = [80, 80, 190]
        color_lose = [190, 80, 80]
        self.text_surface_lose = lose_font.render(self.LOSE_MSG, True, color_lose)
        self.text_surface_msg = self.msg_font.render(self.END_MSG, True, self.color)
        self.text_surface_score = None
        self.lose_position = [
            (Constant.WIN_WIDTH - self.text_surface_lose.get_width()) / 2,
            ((Constant.WIN_HEIGHT - self.text_surface_lose.get_height()) / 2) * 0.9
        ]
        self.msg_position = [
            (Constant.WIN_WIDTH - self.text_surface_msg.get_width()) / 2,
            ((Constant.WIN_HEIGHT - self.text_surface_msg.get_height()) / 2) * 1.1
        ]
        self.score_position = None
        self.start = False
        self.show_msg = True
        self.blink_speed = 500
        self.blink_cooldown = 0

    def update(self, elapsed_time):
        if self.start:
            self.blink_cooldown = 0
            self.show_msg = True
            self.start = False
            return True
        self.blink_cooldown += elapsed_time
        if self.blink_cooldown >= self.blink_speed:
            self.blink_cooldown = 0
            self.show_msg = not self.show_msg
        return False

    def start_game(self):
        self.start = True

    def set_score(self, score):
        self.text_surface_score = self.msg_font.render("Your score : %d" % score, True, self.color)
        self.score_position = [
            (Constant.WIN_WIDTH - self.text_surface_score.get_width()) / 2,
            ((Constant.WIN_HEIGHT - self.text_surface_score.get_height()) / 2) * 1.3
        ]
        self.scores.add_entry(score)

    def draw(self, screen):
        screen.blit(self.text_surface_lose, self.lose_position)
        if self.show_msg:
            screen.blit(self.text_surface_msg, self.msg_position)
        if self.text_surface_score:
            screen.blit(self.text_surface_score, self.score_position)
