##
#  Bullet
##

import pygame
from . import Constant


class Bullet:

    BULLET_SKINS = [
        pygame.transform.smoothscale(pygame.image.load(Constant.BULLET_SKIN_1), [36, 28]),
        pygame.transform.smoothscale(pygame.image.load(Constant.BULLET_SKIN_2), [9, 9]),
        pygame.transform.flip(pygame.transform.smoothscale(pygame.image.load(Constant.BULLET_SKIN_1), [36, 28]), flip_y=False, flip_x=True),
    ]

    def __init__(self, bullet_type, position, speed):
        self.TYPE = bullet_type
        self.position = position
        self.speed = speed
        self.life = self.damage()
        if bullet_type >= len(self.BULLET_SKINS):
            raise Exception("invalid bullet type")

    def update(self, elapsed_time):
        self.position[0] += self.speed[0] * elapsed_time / 1000
        self.position[1] += self.speed[1] * elapsed_time / 1000

    def draw(self, screen):
        if Constant.SHOW_HITBOX:
            pygame.draw.rect(screen, [255, 0, 0], self.get_rect(), width=5)
        screen.blit(self.BULLET_SKINS[self.TYPE], self.get_rect())

    def get_rect(self):
        return pygame.Rect([self.position[0] - self.BULLET_SKINS[self.TYPE].get_width() / 2,
                            self.position[1] - self.BULLET_SKINS[self.TYPE].get_height() / 2],
                           [self.BULLET_SKINS[self.TYPE].get_width(),
                            self.BULLET_SKINS[self.TYPE].get_height()])

    def collision(self, rect):
        return self.get_rect().colliderect(rect)

    def damage(self):
        return Constant.BULLET_DAMAGE[self.TYPE]

    def should_be_deleted(self):
        if self.life <= 0 or \
                self.position[0] + self.BULLET_SKINS[self.TYPE].get_width() / 2 < 0 or \
                self.position[1] - self.BULLET_SKINS[self.TYPE].get_height() / 2 < 0 or \
                self.position[0] - self.BULLET_SKINS[self.TYPE].get_width() / 2 > Constant.WIN_WIDTH or \
                self.position[1] - self.BULLET_SKINS[self.TYPE].get_height() / 2 > Constant.WIN_HEIGHT:
            return True
        return False
