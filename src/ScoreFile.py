##
#  manage score read/write
##

import json
from . import Constant
from pathlib import Path


class ScoreFile:

    def __init__(self):
        self.scores = {
            "bests": [0, 0, 0],
            "last": 0
        }
        if not Path(Constant.SCORE_FILE).is_file():
            with open(Constant.SCORE_FILE, "w") as f:
                json.dump(self.scores, f)
        else:
            with open(Constant.SCORE_FILE, "r") as f:
                self.scores = json.load(f)

    def add_entry(self, score):
        self.scores["last"] = score
        self.scores["bests"].append(score)
        self.scores["bests"].sort(reverse=True)
        with open(Constant.SCORE_FILE, "w") as f:
            json.dump(self.scores, f)

