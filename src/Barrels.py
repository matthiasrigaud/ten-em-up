##
#  define and manage barrels
##

import pygame
from . import Constant, Player
from random import randrange, random
from math import pi, cos, sin


class Barrel:
    BARREL_SKINS = [
        pygame.transform.smoothscale(pygame.image.load(Constant.BARREL_SKIN_1), [30, 36]),
        pygame.transform.smoothscale(pygame.image.load(Constant.BARREL_SKIN_2), [30, 36]),
        pygame.transform.smoothscale(pygame.image.load(Constant.BARREL_SKIN_3), [30, 36]),
        pygame.transform.smoothscale(pygame.image.load(Constant.BARREL_SKIN_4), [30, 36]),
        pygame.transform.smoothscale(pygame.image.load(Constant.BARREL_SKIN_5), [30, 36]),
        pygame.transform.smoothscale(pygame.image.load(Constant.BARREL_SKIN_6), [30, 36]),
    ]

    def __init__(self, position, barrel_type):
        self.TYPE = barrel_type
        self.life = 1
        self.position = position
        prec = 1000000
        angle_speed = randrange(0, int(2 * pi * prec)) / prec
        cos_angle = cos(angle_speed)
        sin_angle = sin(angle_speed)
        self.speed = [
            cos_angle * Constant.BARREL_SPEED - sin_angle * Constant.BARREL_SPEED,
            sin_angle * Constant.BARREL_SPEED + cos_angle * Constant.BARREL_SPEED
        ]

    def update(self, elapsed_time):
        self.position[0] += self.speed[0] * elapsed_time / 1000
        self.position[1] += self.speed[1] * elapsed_time / 1000

    def draw(self, screen):
        if Constant.SHOW_HITBOX:
            pygame.draw.rect(screen, [255, 0, 0], self.get_rect(), width=5)
        screen.blit(self.BARREL_SKINS[self.TYPE], self.get_rect())

    def get_rect(self):
        return pygame.Rect([
            self.position[0] - self.BARREL_SKINS[self.TYPE].get_width() / 2,
            self.position[1] - self.BARREL_SKINS[self.TYPE].get_height() / 2,
        ], [self.BARREL_SKINS[self.TYPE].get_width(), self.BARREL_SKINS[self.TYPE].get_height()])

    def collision(self, rect):
        return self.get_rect().colliderect(rect)

    def should_be_deleted(self):
        if self.life <= 0 or \
                self.position[0] + self.BARREL_SKINS[self.TYPE].get_width() / 2 < 0 or \
                self.position[1] + self.BARREL_SKINS[self.TYPE].get_height() / 2 < 0 or \
                self.position[0] - self.BARREL_SKINS[self.TYPE].get_width() / 2 > Constant.WIN_WIDTH or \
                self.position[1] - self.BARREL_SKINS[self.TYPE].get_height() / 2 > Constant.WIN_HEIGHT:
            return True
        return False


class Effect:

    def __init__(self, description, effect, good=True):
        self.description = description
        self.effect = effect
        self.GOOD = good

    def apply_effect(self, player):
        self.effect(player)


##
# possible effects
##


def will_heal_you(player):
    player.life += Constant.LIFE_EFFECT
    if player.life > Constant.MAX_PLAYER_LIFE:
        player.life = Constant.MAX_PLAYER_LIFE


def will_hurt_you(player):
    player.take_damage(Constant.LIFE_EFFECT)


def will_improve_weapon(player):
    player.type_weapon = (player.type_weapon + 1) % 2
    player.bullet_quantity[player.type_weapon] += Constant.BULLET_QTTY[player.type_weapon]
    player.bullet_speed[player.type_weapon][0] *= Constant.WEAPON_SPEED_COEF
    player.shoot_speed /= Constant.SHOOT_SPEED_COEF if player.type_weapon == 0 else 1
    player.weapon_level += 1


def will_downgrade_weapon(player):
    if player.weapon_level > 0:
        player.weapon_level -= 1
        player.shoot_speed /= 1 / Constant.SHOOT_SPEED_COEF if player.type_weapon == 1 else 1
        player.bullet_speed[player.type_weapon][0] *= 1 / Constant.WEAPON_SPEED_COEF
        player.bullet_quantity[player.type_weapon] -= Constant.BULLET_QTTY[player.type_weapon]
        player.type_weapon = (player.type_weapon + 1) % 2


def will_freeze_weapon(player):
    player.shoot_cooldown -= Constant.FREEZE_EFFECT


def will_slow_you_down(player):
    player.speed *= 1 / Constant.PLAYER_SPEED_COEF


def will_speed_you_up(player):
    player.speed *= Constant.PLAYER_SPEED_COEF


class Interface:
    EFFECTS = [
        Effect("    heal you    ", will_heal_you),
        Effect("    hurt you    ", will_hurt_you, False),
        Effect(" improve weapon ", will_improve_weapon),
        Effect("downgrade weapon", will_downgrade_weapon, False),
        Effect("  freeze weapon ", will_freeze_weapon, False),
        Effect("  slow you down ", will_slow_you_down, False),
        Effect("  speed you up  ", will_speed_you_up),
    ]

    def __init__(self):
        self.current_effects = []
        self.change_effects()
        self.font = pygame.font.Font(Constant.FONT_PATH, 10)
        self.barrels = []

    def spawn_barrel(self, position):
        self.barrels.append(Barrel(position, randrange(0, Constant.MAX_BARRELS)))

    def update(self, elapsed_time):
        new_barrels = []
        for barrel in self.barrels:
            barrel.update(elapsed_time)
            if not barrel.should_be_deleted():
                new_barrels.append(barrel)
        self.barrels = new_barrels

    def change_effects(self):
        self.current_effects.clear()
        used_effects = []
        for i in range(Constant.MAX_BARRELS):
            have_to_be_bad = True if random() <= Constant.BAD_COEF else False
            while True:
                effect = randrange(0, len(self.EFFECTS))
                if effect not in used_effects:
                    if (have_to_be_bad and not self.EFFECTS[effect].GOOD) or not have_to_be_bad:
                        break
            used_effects.append(effect)

            self.current_effects.append(effect)

    def draw_barrels(self, screen):
        for barrel in self.barrels:
            barrel.draw(screen)

    def collide_with_barrels(self, player):
        for barrel in self.barrels:
            if barrel.collision(player.get_rect()):
                self.EFFECTS[self.current_effects[barrel.TYPE]].apply_effect(player)
                barrel.life = 0

    def draw_interface(self, screen, position):
        for i in range(len(self.current_effects)):
            color = [70, 70, 70]
            if self.EFFECTS[self.current_effects[i]].GOOD:
                color[1] += 80
            else:
                color[0] += 80
            text_surface_will = self.font.render("Will", True, color)
            text_surface = self.font.render(self.EFFECTS[self.current_effects[i]].description, True, color)
            position_will = list(position)
            position_will[0] += (text_surface.get_width() - text_surface_will.get_width()) / 2
            position_desc = list(position)
            position_desc[1] += text_surface_will.get_height() * 1.5
            position_barrel = list(position_desc)
            position_barrel[1] += text_surface.get_height() * 1.8
            position_barrel[0] += (text_surface.get_width() - Barrel.BARREL_SKINS[i].get_width()) / 2
            screen.blit(text_surface_will, position_will)
            screen.blit(text_surface, position_desc)
            screen.blit(Barrel.BARREL_SKINS[i], position_barrel)
            position[0] += text_surface.get_width() + 20
