##
#  Event Manager
##

import pygame
import sys
from . import StartScreen


def manager(end_start_state):
    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.dict["key"] == pygame.K_ESCAPE):
            sys.exit()
        elif end_start_state and (event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN):
            end_start_state.start_game()
