##
#  Ten seconds timer manager
##

import pygame
from . import Constant
from math import pi, ceil


class TenSeconds:

    timer = None
    activate_event = None
    emptying = False

    MAX_TIMER = 10 * 1000  # we work with milliseconds
    HALF_PI = 0.5 * pi
    TWO_PI = 2 * pi

    def __init__(self):
        self.font = pygame.font.Font(Constant.FONT_PATH, 40)
        self.reset()

    def reset(self):
        self.timer = self.MAX_TIMER
        self.activate_event = False
        self.emptying = not self.emptying

    def update(self, elapsed_time):
        self.timer -= elapsed_time
        if self.timer <= 0:
            self.timer = 0
            self.activate_event = True

    def event(self):
        return self.activate_event

    def draw(self, screen, position):
        timer_color = [0, 0, 0]

        if self.timer > 5 * 1000:
            timer_color[1] = 255
            timer_color[0] = 255 - 255 * (self.timer - self.MAX_TIMER / 2) / (self.MAX_TIMER / 2)
        else:
            timer_color[0] = 255
            timer_color[1] = 255 * self.timer / (self.MAX_TIMER / 2)

        text_surface = self.font.render("%02i" % ceil(self.timer / 1000), True, timer_color)

        timer_rect = text_surface.get_rect()
        timer_rect.x = position[0]
        timer_rect.y = position[1]

        timer_rect_copy = timer_rect.copy()

        old_height = timer_rect_copy.height
        timer_rect_copy.height *= 2.2
        timer_rect.y += (timer_rect_copy.height - old_height) / 2

        old_width = timer_rect_copy.width
        timer_rect_copy.width *= 1.3
        timer_rect.x += (timer_rect_copy.width - old_width) / 2

        start_angle = self.HALF_PI - (self.TWO_PI * self.timer) / self.MAX_TIMER
        stop_angle = self.HALF_PI

        if not self.emptying:
            tmp_angle = start_angle
            start_angle = stop_angle
            stop_angle = tmp_angle

        screen.blit(text_surface, timer_rect)
        pygame.draw.arc(screen, timer_color, timer_rect_copy, start_angle, stop_angle, 3)
