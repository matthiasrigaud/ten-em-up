#### CONSTANT GLOBAL VALUES ####

from math import pi

##
#  window
##

WIN_WIDTH       = 1280
WIN_HEIGHT      = 720
WIN_TITLE       = "Ten 'em up"

##
#  frame
##

FRAME_TIME      = 35

##
#  assets
##

ASSETS_DIR      = "./assets/"
FONT_DIR        = ASSETS_DIR + "font/"
FONT_PATH       = FONT_DIR + "ARCADE_R.TTF"
MUSIC_PATH      = ASSETS_DIR + "ten_seconds_journey.wav"

##
#  barrels
##

BARREL_SKIN_1   = ASSETS_DIR + "barrel_1.png"
BARREL_SKIN_2   = ASSETS_DIR + "barrel_2.png"
BARREL_SKIN_3   = ASSETS_DIR + "barrel_3.png"
BARREL_SKIN_4   = ASSETS_DIR + "barrel_4.png"
BARREL_SKIN_5   = ASSETS_DIR + "barrel_5.png"
BARREL_SKIN_6   = ASSETS_DIR + "barrel_6.png"

MAX_BARRELS     = 4

BARREL_SPEED    = 200

##
#  player
##

PLAYER_SKIN     = ASSETS_DIR + "player.png"
MAX_PLAYER_LIFE = 15

##
#  bullets
##

BULLET_SKIN_1   = ASSETS_DIR + "bullet_1.png"
BULLET_SKIN_2   = ASSETS_DIR + "bullet_2.png"
BULLET_DAMAGE   = [
    2,
    1,
    2
]

##
#  sky
##

SKY_COLOR       = [76, 198, 235]
CLOUD_COEF      = 0.002  # probability that a cloud will spawn each ms
CLOUD_MIN_SPEED = 80  # pxls/sec
CLOUD_MAX_SPEED = 200
MAX_CLOUDS      = 15
CLOUD_1         = ASSETS_DIR + "cloud_1.png"
CLOUD_2         = ASSETS_DIR + "cloud_2.png"
CLOUD_3         = ASSETS_DIR + "cloud_3.png"
CLOUD_4         = ASSETS_DIR + "cloud_4.png"

##
#  enemies
##

ENEMY_SKIN_1    = ASSETS_DIR + "enemy.png"
ENEMY_SKIN_2    = ASSETS_DIR + "enemy_small.png"
ENEMY_COEF      = 0.0006
ENEMY_ACT_FRQ   = [
    1000,  # enemy 1 will act every second
    1500   # enemy 2 will act every 2 seconds
]
ENEMY_LIFE      = [
    6,  # enemy 1
    3   # enemy 2
]
ENEMY_SHIFT_SPD = 500
ENEMY_LOOT      = [
    3,
    1
]

##
#  effects
##

# 50% chances that it will be bad, 50% chances that it will be good or bad
BAD_COEF            = 0

SHOOT_SPEED_COEF    = 1.05
WEAPON_SPEED_COEF   = 1.05
PLAYER_SPEED_COEF   = 1.25
LIFE_EFFECT         = 5
FREEZE_EFFECT       = 5000  # you can't shoot 5 seconds
BULLET_QTTY         = [1, 2]

##
#  score
##

SCORE_KILL          = [
    150,
    50
]
SCORE_TEN_SEC       = 100
SCORE_TEN_SEC_COEF  = 1.1
SCORE_FILE          = ASSETS_DIR + "scores.json"

##
#  debug
##

SHOW_HITBOX = False
