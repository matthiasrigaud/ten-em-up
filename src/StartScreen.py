##
#  screen displayed at beggining
##

import pygame
from . import Constant, ScoreFile


class StartScreen:

    START_MSG = "Press start!"

    def __init__(self, scores):
        self.scores = scores
        title_font = pygame.font.Font(Constant.FONT_PATH, 90)
        self.msg_font = pygame.font.Font(Constant.FONT_PATH, 25)
        self.color = [80, 80, 190]
        self.text_surface_title = title_font.render(Constant.WIN_TITLE, True, self.color)
        self.text_surface_msg = self.msg_font.render(self.START_MSG, True, self.color)
        self.title_position = [
            (Constant.WIN_WIDTH - self.text_surface_title.get_width()) / 2,
            ((Constant.WIN_HEIGHT - self.text_surface_title.get_height()) / 2) * 0.9
        ]
        self.msg_position = [
            (Constant.WIN_WIDTH - self.text_surface_msg.get_width()) / 2,
            ((Constant.WIN_HEIGHT - self.text_surface_msg.get_height()) / 2) * 1.1
        ]
        self.start = False
        self.show_msg = True
        self.blink_speed = 500
        self.blink_cooldown = 0

    def update(self, elapsed_time):
        if self.start:
            self.start = False
            self.show_msg = True
            self.blink_cooldown = 0
            return True
        self.blink_cooldown += elapsed_time
        if self.blink_cooldown >= self.blink_speed:
            self.blink_cooldown = 0
            self.show_msg = not self.show_msg
        return False

    def start_game(self):
        self.start = True

    def draw(self, screen):
        screen.blit(self.text_surface_title, self.title_position)
        if self.show_msg:
            screen.blit(self.text_surface_msg, self.msg_position)
        text_surface_score = self.msg_font.render("Top Score :", True, self.color)
        score_position = [
            self.title_position[0] + 10,
            ((Constant.WIN_HEIGHT - text_surface_score.get_height()) / 2) * 1.25
        ]

        screen.blit(text_surface_score, score_position)
        score_position[1] += 15

        for i in range(3):
            text_surface_score = self.msg_font.render("%d - %d" % (i + 1, self.scores.scores["bests"][i]), True, self.color)
            score_position[1] += text_surface_score.get_height() * 1.2
            screen.blit(text_surface_score, score_position)

        text_surface_score = self.msg_font.render("Last Score :", True, self.color)
        score_position = [
            self.title_position[0] + self.text_surface_title.get_width() - text_surface_score.get_width(),
            ((Constant.WIN_HEIGHT - text_surface_score.get_height()) / 2) * 1.25
        ]

        screen.blit(text_surface_score, score_position)

        text_surface_score = self.msg_font.render("%d" % self.scores.scores["last"], True, self.color)
        score_position[1] += text_surface_score.get_height() * 1.2 + 15

        screen.blit(text_surface_score, score_position)
