![Ten 'em up](https://gitlab.com/matthiasrigaud/ten-em-up/-/raw/master/assets/cover.png)

## Instructions

Uses Python3 and Pygame to work, so Linux users must install them (use your package manager for python, i. e. `pacman -S python3`, and pip to install the requirements `pip install -r requirements.txt`).

A stand-alone executable for Windows will be available soon.

## Story

You're on a mission to exterminate your flying enemies and steal them their equipment. The only problem ? They trapped their equipment with some malicious stuff, and change packaging every 10 seconds to confuse you. But do not worry, your radar will indicate you on time what contains their dropped equipment. Just be careful, mistakes could be irrecoverable ... 

## How to play

You control your ship with the mouse cursor, that's all.

### Barrels

Your enemies drop barrels containing good and bad things. The content of the barrels change every 10 seconds and is indicated at the top of your screen. You can read next what can be in the barrels, or just wait and discover directly in-game.

Here is a list of what you could find in barrels :

#### Good things :
* heal
* weapon upgrade
* ship speed-up

#### Bad things :
* (big) damages
* weapon downgrade
* freeze weapon for 5 secs
* ship slow-down

That's all, enjoy the game : )

## Notes
#### Used tools :
 * [Python3](https://www.python.org/)
 * [Pygame](https://www.pygame.org)
 * [Gimp](https://www.gimp.org/)
 * [Audacity](https://www.audacityteam.org/)
 * [craiyon](https://www.craiyon.com/) (to generate cloud sprites)

#### Font is taken from internet. See the [Readme.TXT](https://gitlab.com/matthiasrigaud/ten-em-up/-/blob/master/assets/font/Readme.TXT) in assets/font to learn more

## LD51

This game was made for the LD51 Compo : [LD51 Profile](https://ldjam.com/events/ludum-dare/51/ten-em-up).